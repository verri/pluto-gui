#include "entrytable.h"

#include <QPushButton>

#include "entryitemdelegator.h"

EntryTable::EntryTable(QWidget *parent) :
    QTableWidget{0, COLUMNS_TOTAL, parent}
{
    setHorizontalHeaderLabels(QStringList{
        "id", "", "", "Date", "Account", "Tag", "Description",
        "Quantity", "Value", "Total"
    });
    setColumnHidden(0, true);
    setEditTriggers(EditTrigger::AllEditTriggers);
    setItemDelegate(new EntryItemDelegator);

    resizeColumnsToContents();

    connect(this, SIGNAL(cellChanged(int,int)), this, SLOT(check_cell(int, int)));
}

void EntryTable::check_cell(int i, int j)
{
    resizeColumnsToContents();

    auto total = item(i, TOTAL_COLUMN);
    auto quantity = item(i, QUANTITY_COLUMN);
    auto value = item(i, VALUE_COLUMN);

    if (!total || !quantity || !value)
        return;

    if (j == QUANTITY_COLUMN || j == VALUE_COLUMN)
        total->setText(QString().setNum(
            quantity->text().toFloat() * value->text().toFloat()
        ));
    else if (j == TOTAL_COLUMN && quantity->text().toFloat() != 0.0f)
        value->setText(QString().setNum(
            total->text().toFloat() / quantity->text().toFloat()
        ));
    else if (j == TOTAL_COLUMN)
        total->setText("0");
}

void EntryTable::closeEditor(QWidget * editor, QAbstractItemDelegate::EndEditHint hint)
{
    if (QAbstractItemDelegate::EditNextItem &&
        currentColumn() == columnCount() - 1 &&
        currentRow() == rowCount() - 1)
        new_entry();
    QTableWidget::closeEditor(editor, hint);
}

void EntryTable::new_entry()
{
    int row_count = rowCount();
    insertRow(row_count);

    // TODO: setUserData(row_count, jmanager);

    setCellWidget(row_count, SYNC_COLUMN, new QPushButton);
    setCellWidget(row_count, REMOVE_COLUMN, new QPushButton);

    setItem(row_count, DATE_COLUMN, new QTableWidgetItem);
    setItem(row_count, ACCOUNT_COLUMN, new QTableWidgetItem);
    setItem(row_count, QUANTITY_COLUMN, new QTableWidgetItem);
    setItem(row_count, VALUE_COLUMN, new QTableWidgetItem);
    setItem(row_count, TOTAL_COLUMN, new QTableWidgetItem);

    auto first = item(row_count, DATE_COLUMN);

    scrollToItem(first);
    setCurrentItem(first);
    editItem(first);

    resizeColumnsToContents();
}
