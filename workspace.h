#ifndef WORKSPACE_H
#define WORKSPACE_H

#include <QTabWidget>

class QShortcut;

class Workspace : public QTabWidget
{
    Q_OBJECT
public:
    explicit Workspace(QWidget *parent = 0);
    ~Workspace();

private slots:
    void closeTab(int index);
    void closeCurrentTab();

public slots:

private:
    QShortcut *close_tab;
};

#endif // WORKSPACE_H
