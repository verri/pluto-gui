#include "workspace.h"

#include <QShortcut>

Workspace::Workspace(QWidget *parent) :
    QTabWidget{parent}
{
    setTabsClosable(true);
    connect(this, SIGNAL(tabCloseRequested(int)),
            this, SLOT(closeTab(int)));

    close_tab = new QShortcut{
        QKeySequence{Qt::CTRL + Qt::Key_W},
        this, SLOT(closeCurrentTab())
    };
}

Workspace::~Workspace()
{
    // TODO: close tabs...
    delete close_tab;
}

void Workspace::closeTab(int index)
{
    // TODO: check modifications
    auto w = widget(index);
    removeTab(index);
    delete w;
}

void Workspace::closeCurrentTab()
{
    int index = currentIndex();
    closeTab(index);
}
