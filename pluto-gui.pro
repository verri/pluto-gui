#-------------------------------------------------
#
# Project created by QtCreator 2014-07-11T10:24:23
#
#-------------------------------------------------

QT       += core gui
QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = plgui
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    journalmanager.cpp \
    workspace.cpp \
    entrypage.cpp \
    entrytable.cpp \
    entryitemdelegator.cpp

HEADERS  += mainwindow.h \
    journalmanager.h \
    workspace.h \
    entrypage.h \
    entrytable.h \
    entryitemdelegator.h

FORMS    += mainwindow.ui \
    entrypage.ui

LIBS     += -lpluto -lsqlite3
