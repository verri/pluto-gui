#ifndef ENTRYTABLE_H
#define ENTRYTABLE_H

#include <QTableWidget>

class EntryTable : public QTableWidget
{
    Q_OBJECT
public:
    explicit EntryTable(QWidget *parent = nullptr);

    void new_entry();

signals:

public slots:

protected slots:
    void closeEditor(QWidget * editor, QAbstractItemDelegate::EndEditHint hint);

private slots:
    void check_cell(int i, int j);
};

#endif // ENTRYTABLE_H
