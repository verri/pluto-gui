#include "entrypage.h"
#include "ui_entrypage.h"

EntryPage::EntryPage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EntryPage)
{
    ui->setupUi(this);

    ui->pb_new_entry->insertAction(nullptr, ui->action_new_entry);
}

EntryPage::~EntryPage()
{
    delete ui;
}

void EntryPage::on_action_new_entry_triggered()
{
    ui->table_entries->new_entry();
}
