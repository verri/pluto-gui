#include "entryitemdelegator.h"

#include <QTableWidgetItem>
#include <QDateEdit>
#include <QDoubleSpinBox>
#include <QPainter>

EntryItemDelegator::EntryItemDelegator(QObject *parent) :
    QStyledItemDelegate(parent)
{
}

QWidget *EntryItemDelegator::createEditor(
    QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QWidget *editor;

    switch (index.column())
    {
    case DATE_COLUMN:
        editor = new QDateEdit{QDate::currentDate(), parent};
        static_cast<QDateEdit *>(editor)->setDisplayFormat("dd/MMM/yyyy");
        break;
    case QUANTITY_COLUMN:
    case VALUE_COLUMN:
    case TOTAL_COLUMN:
        editor = new QDoubleSpinBox{parent};
        static_cast<QDoubleSpinBox *>(editor)->setDecimals(3);
        break;
    default:
        editor = QStyledItemDelegate::createEditor(parent, option, index);
    }

    return editor;
}

void EntryItemDelegator::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QString data;

    switch (index.column())
    {
    case DATE_COLUMN:
        data = static_cast<QDateEdit *>(editor)->date().toString("dd/MMM/yyyy");
        model->setData(index, data, Qt::DisplayRole);
        break;
    default:
        QStyledItemDelegate::setModelData(editor, model, index);
    }
}
