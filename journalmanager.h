#ifndef JOURNALMANAGER_H
#define JOURNALMANAGER_H

#include <QObject>
#include <QMap>

namespace pl
{
    class Journal;
    class Entry;
}

class JournalManager : public QObject
{
    Q_OBJECT
public:
    explicit JournalManager(QObject *parent = 0);

    void open(const QString& filename);

signals:

public slots:

private:
    pl::Journal *journal;
    QMap<long, pl::Entry *> entries;
};

#endif // JOURNALMANAGER_H
