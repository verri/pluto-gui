#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QSettings;
class JournalManager;
class Workspace;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_action_New_Journal_triggered();

    void on_action_Open_Journal_triggered();

    void on_action_Create_New_List_triggered();

private:
    void openJournal(const QString& filename);
    void setupMenu();
    void setupToolbar();

    Ui::MainWindow *ui;

    QSettings *settings;
    JournalManager *jmanager;
    Workspace *workspace;
};

#endif // MAINWINDOW_H
