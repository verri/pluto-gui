#ifndef ENTRYPAGE_H
#define ENTRYPAGE_H

#include <QWidget>

namespace Ui {
class EntryPage;
}

class EntryPage : public QWidget
{
    Q_OBJECT

public:
    explicit EntryPage(QWidget *parent = 0);
    ~EntryPage();

private slots:
    void on_action_new_entry_triggered();

private:
    Ui::EntryPage *ui;
};

#endif // ENTRYPAGE_H
