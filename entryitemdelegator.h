#ifndef ENTRYITEMDELEGATOR_H
#define ENTRYITEMDELEGATOR_H

#include <QStyledItemDelegate>

enum
{
    ID_COLUMN = 0,
    SYNC_COLUMN,
    REMOVE_COLUMN,
    DATE_COLUMN,
    ACCOUNT_COLUMN,
    TAG_COLUMN,
    DESCRIPTION_COLUMN,
    QUANTITY_COLUMN,
    VALUE_COLUMN,
    TOTAL_COLUMN,
    COLUMNS_TOTAL
};

class EntryItemDelegator : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit EntryItemDelegator(QObject *parent = 0);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

signals:

public slots:

};

#endif // ENTRYITEMDELEGATOR_H
