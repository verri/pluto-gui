#include "journalmanager.h"
#include "pluto.h"

JournalManager::JournalManager(QObject *parent) :
    QObject{parent}, journal{nullptr}
{
}

void JournalManager::open(const QString &filename)
{
    if (journal)
        delete journal;
    journal = new pl::Journal{filename.toStdString()};
}
