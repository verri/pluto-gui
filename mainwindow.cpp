#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>

#include "journalmanager.h"
#include "workspace.h"
#include "entrypage.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow{parent},
    ui{new Ui::MainWindow},
    settings{new QSettings{"Verri", "Pluto"}}
{
    ui->setupUi(this);

    jmanager = new JournalManager{this};
    workspace = new Workspace{this};

    setCentralWidget(workspace);
    setupMenu();
    setupToolbar();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openJournal(const QString& filename)
{
    if (filename.isEmpty())
        return;

    QFileInfo fileInfo{filename};
    if (fileInfo.exists() && !fileInfo.isWritable()) {
        QMessageBox::warning(this, "Warning!", "File is not writable.");
        return;
    }
    if (fileInfo.exists() && !fileInfo.isReadable()) {
        QMessageBox::warning(this, "Warning!", "File is not readable");
        return;
    }

    jmanager->open(filename);

    settings->setValue("LastPath", fileInfo.absolutePath());

    QSet<QString> recentJournals;
    int size = settings->beginReadArray("RecentJournals");
    for (int i = 0; i < size; ++i) {
        settings->setArrayIndex(i);
        recentJournals << settings->value("Filename").toString();

    }
    recentJournals << fileInfo.canonicalFilePath();
    settings->endArray();

    auto journals = recentJournals.begin();
    settings->beginWriteArray("RecentJournals");
    for (int i = 0; journals != recentJournals.end(); ++i) {
        settings->setArrayIndex(i);
        settings->setValue("Filename", *journals++);
    }
    settings->endArray();

    setWindowTitle(fileInfo.completeBaseName() + " - Pluto");

    ui->action_New_Journal->setEnabled(false);
    ui->action_Open_Journal->setEnabled(false);
}

void MainWindow::setupMenu()
{
    connect(ui->action_Quit, SIGNAL(triggered()), qApp, SLOT(quit()));
}

void MainWindow::setupToolbar()
{
    ui->mainToolBar->insertAction(nullptr, ui->action_New_Journal);
    ui->mainToolBar->insertAction(nullptr, ui->action_Open_Journal);
    // TODO
}

void MainWindow::on_action_New_Journal_triggered()
{
    auto filename = QFileDialog::getSaveFileName(this, "Create new journal",
                                                 settings->value("LastPath").toString(),
                                                 "Pluto Journal (*.plj);; All Files (*)");

    if (filename.isEmpty())
        return;

    QFileInfo fileInfo{filename};
    if (fileInfo.completeSuffix().isEmpty())
        filename.append(".plj");
    openJournal(filename);
}

void MainWindow::on_action_Open_Journal_triggered()
{
    auto filename = QFileDialog::getOpenFileName(this, "Open journal",
                                                 settings->value("LastPath").toString(),
                                                 "Pluto Journal (*.plj);; All Files (*)");

    openJournal(filename);
}

void MainWindow::on_action_Create_New_List_triggered()
{
    auto index = workspace->addTab(new EntryPage{}, "");
    workspace->setTabText(index, "List " + QString().setNum(index));
}
